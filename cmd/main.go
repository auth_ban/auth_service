package main

import (
	"log"
	"net"
	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	_ "github.com/lib/pq"
	pb "gitlab.com/auth_ban/auth_service/internal/genproto/auth_service"
	"gitlab.com/auth_ban/auth_service/internal/grpc_client"
	configs "gitlab.com/auth_ban/auth_service/internal/pkg/config"
	"gitlab.com/auth_ban/auth_service/internal/pkg/repository"
	"gitlab.com/auth_ban/auth_service/internal/pkg/utils"
	"gitlab.com/auth_ban/auth_service/internal/repository/postgres/register"
	register2 "gitlab.com/auth_ban/auth_service/internal/service/register"
	register3 "gitlab.com/auth_ban/auth_service/internal/usecase/register"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	appConfig   = configs.Config()
)

// @title API
// @version 1
// @description This is an auto-generated API Docs.
// @termsOfService http://swagger.io/terms/
// @contact.name Soburjon
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {

	// database
	postgresDB := repository.NewPostgres()
	
	registerRepo := register.NewRepository(postgresDB)

	//service
	registerService := register2.NewService(registerRepo)

	client, err := grpcclient.New(*appConfig)
	if err != nil {
		log.Fatal("error while connecting other services")
		return
	}
	// use case
	registerUseCase := register3.NewUseCase(registerService,client)

	// controller

	utils.MigrationsUp()

	lis, err := net.Listen("tcp", appConfig.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %w", err.Error())
	}
	s := grpc.NewServer()
	pb.RegisterAuthServiceServer(s, registerUseCase)
	reflection.Register(s)

	print("main: server running "+appConfig.AuthServiceHost+appConfig.RPCPort)

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %w", err.Error())
	}

}
