package grpcclient

import (
	"fmt"

	"gitlab.com/auth_ban/auth_service/internal/pkg/config"
	pb "gitlab.com/auth_ban/auth_service/internal/genproto/auth_service"
	"google.golang.org/grpc"
)

//IServiceManager ...
type IServiceManager interface {
	AuthService() pb.AuthServiceClient
}

type serviceManager struct {
	cfg          config.Configuration
	authService  pb.AuthServiceClient
}

//New ...
func New(cfg config.Configuration) (IServiceManager, error) {
	connAuth, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.AuthServiceHost, cfg.AuthServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %s port: %d",
			cfg.AuthServiceHost, cfg.AuthServicePort)
	}

	serviceManager := &serviceManager{
		cfg:          cfg,
		authService:  pb.NewAuthServiceClient(connAuth),
	}

	return serviceManager, nil
}

// UserService ...
func (s *serviceManager) AuthService() pb.AuthServiceClient {
	return s.authService
}