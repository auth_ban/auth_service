package register

import (
	"context"
	"github.com/uptrace/bun"
	"gitlab.com/auth_ban/auth_service/internal/entity"
	"gitlab.com/auth_ban/auth_service/internal/service/register"
)

type registerRepo struct {
	*bun.DB
}

func NewRepository(DB *bun.DB) *registerRepo {
	return &registerRepo{
		DB,
	}
}

func (r *registerRepo) Login(req register.RegisterRequest) (entity.Users, error) {
	user := new(entity.Users)
	err := r.NewSelect().Model(user).Where("phone_number = ? and password = ? and deleted_at IS NULL", req.PhoneNumber, req.Password).Scan(context.Background())
	if err != nil {
		return entity.Users{}, err
	}
	return *user, nil
}

func (a *registerRepo) CreateUser(req entity.Users) error {
	_, err := a.NewInsert().Model(&req).Exec(context.Background())
	if err != nil {
		return err
	}
	return nil
}