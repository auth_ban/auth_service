#!/usr/bin/env sh
set -e

>&2 echo "Running unit tests ..."
CGO_ENABLED=0 go test ./... -v -mod=vendor

>&2 echo "Running integration tests ..."
cd ./tests
CGO_ENABLED=0 go test ./... -v -mod=vendor -tags integration