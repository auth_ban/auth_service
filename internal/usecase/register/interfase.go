package register

import (
	"gitlab.com/auth_ban/auth_service/internal/entity"
	"gitlab.com/auth_ban/auth_service/internal/service/register"
)

type RegisterService interface {
	CreateUser(req entity.Users) error
	Login(req register.RegisterRequest) (entity.Users, error)
}
