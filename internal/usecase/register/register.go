package register

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/auth_ban/auth_service/internal/entity"
	pb "gitlab.com/auth_ban/auth_service/internal/genproto/auth_service"
	"gitlab.com/auth_ban/auth_service/internal/grpc_client"
	"gitlab.com/auth_ban/auth_service/internal/service/register"
)

type RegisterUseCase struct {
	service RegisterService
	client grpcclient.IServiceManager
}

func NewUseCase(s RegisterService,c grpcclient.IServiceManager) *RegisterUseCase {
	return &RegisterUseCase{
		service: s,
		client: c,
	}
}

func (r *RegisterUseCase) LogIn(ctx context.Context,req *pb.LogInRequest) (*pb.LogInResponse, error) {
	res, err := r.service.Login(register.RegisterRequest{
		PhoneNumber: req.PhoneNumber,
		Password: req.Password,
	})
	if err != nil {
		return &pb.LogInResponse{}, err
	}
	return &pb.LogInResponse{
		UserID: res.ID,
		Role: res.Role,
	}, nil
}

func (r *RegisterUseCase) CreateUser(ctx context.Context,req *pb.CreateUserRequest) (*pb.Empty,error) {
	user := entity.Users{}
	user.ID = uuid.NewString()
	user.FullName = req.FullName
	user.Avatar = &req.Avatar
	user.PhoneNumber = req.PhoneNumber
	user.Birthday = req.Birthday
	user.Password = req.Password
	user.Position = req.Position
	user.Role = req.Role
	err:=r.service.CreateUser(user)
	return &pb.Empty{},err
}
