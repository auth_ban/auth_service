package register

import (
	"gitlab.com/auth_ban/auth_service/internal/entity"
)

type RegisterRepo interface {
	Login(req RegisterRequest) (entity.Users, error)
	CreateUser(req entity.Users) error
}
