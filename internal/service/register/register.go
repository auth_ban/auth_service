package register

import (
	"gitlab.com/auth_ban/auth_service/internal/entity"
)

type RegisterService struct {
	repo RegisterRepo
}

func NewService(repo RegisterRepo) *RegisterService {
	return &RegisterService{
		repo: repo,
	}
}

func (r *RegisterService) Login(req RegisterRequest) (entity.Users, error) {
	return r.repo.Login(req)
}

func (a *RegisterService) CreateUser(req entity.Users) error {
	return a.repo.CreateUser(req)
}
