CREATE TABLE IF NOT EXISTS users (
    id UUID NOT NULL PRIMARY KEY,
    full_name TEXT NOT NULL,
    avatar TEXT,
    role TEXT NOT NULL,
    phone_number TEXT NOT NULL UNIQUE,
    birthday TIMESTAMP NOT NULL,
    password TEXT NOT NULL,
    position TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP DEFAULT NULL
);